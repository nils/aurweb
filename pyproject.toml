[tool.black]
line-length = 88

[tool.isort]
profile = "black"
combine_as_imports = true


# Poetry build configuration for the aurweb project.
#
# Dependencies:
#   * python >= 3.9
#   * pip
#       * poetry
#       * poetry-dynamic-versioning
#
[tool.poetry]
name = "aurweb"
version = "v6.2.4"
license = "GPL-2.0-only"
description = "Source code for the Arch User Repository's website"
homepage = "https://aur.archlinux.org"
repository = "https://gitlab.archlinux.org/archlinux/aurweb"
documentation = "https://gitlab.archlinux.org/archlinux/aurweb/-/blob/master/README.md"
keywords = ["aurweb", "aur", "Arch", "Linux"]
authors = [
    "Lucas Fleischer <lfleischer@archlinux.org>",
    "Eli Schwartz <eschwartz@archlinux.org>",
    "Kevin Morris <kevr@0cost.org>"
]
maintainers = [
    "Eli Schwartz <eschwartz@archlinux.org>"
]
packages = [
    { include = "aurweb" }
]

[tool.poetry-dynamic-versioning]
enable = true
vcs = "git"

[build-system]
requires = ["poetry>=1.1.8", "poetry-dynamic-versioning"]
build-backend = "poetry.masonry.api"

[tool.poetry.urls]
"Repository" = "https://gitlab.archlinux.org/archlinux/aurweb"
"Bug Tracker" = "https://gitlab.archlinux.org/archlinux/aurweb/-/issues"
"Development Mailing List" = "https://lists.archlinux.org/listinfo/aur-dev"
"General Mailing List" = "https://lists.archlinux.org/listinfo/aur-general"
"Request Mailing List" = "https://lists.archlinux.org/listinfo/aur-requests"

[tool.poetry.dependencies]
python = ">=3.9,<3.12"

# poetry-dynamic-versioning is used to produce tool.poetry.version
# based on git tags.

# General
aiofiles = "^23.1.0"
asgiref = "^3.6.0"
bcrypt = "^4.0.1"
bleach = "^6.0.0"
email-validator = "^2.0.0-post.0"
fakeredis = "^2.11.2"
feedgen = "^0.9.0"
httpx = "^0.24.0"
itsdangerous = "^2.1.2"
lxml = "^4.9.2"
orjson = "^3.8.11"
protobuf = "^4.22.4"
pygit2 = "^1.12.0"
python-multipart = "^0.0.6"
redis = "^4.5.4"
requests = "^2.30.0"
paginate = "^0.5.6"

# SQL
alembic = "^1.10.4"
mysqlclient = "^2.1.1"
Authlib = "^1.2.0"
Jinja2 = "^3.1.2"
Markdown = "^3.4.3"
Werkzeug = "^2.3.3"
SQLAlchemy = "^1.4.48"

# ASGI
uvicorn = "^0.22.0"
gunicorn = "^20.1.0"
Hypercorn = "^0.14.3"
prometheus-fastapi-instrumentator = "^6.0.0"
pytest-xdist = "^3.2.1"
filelock = "^3.12.0"
posix-ipc = "^1.1.1"
pyalpm = "^0.10.6"
fastapi = "^0.95.1"
srcinfo = "^0.1.2"
tomlkit = "^0.11.8"

[tool.poetry.dev-dependencies]
coverage = "^7.2.5"
pytest = "^7.3.1"
pytest-asyncio = "^0.21.0"
pytest-cov = "^4.0.0"
pytest-tap = "^3.3"
watchfiles = "^0.19.0"

[tool.poetry.scripts]
aurweb-git-auth = "aurweb.git.auth:main"
aurweb-git-serve = "aurweb.git.serve:main"
aurweb-git-update = "aurweb.git.update:main"
aurweb-aurblup = "aurweb.scripts.aurblup:main"
aurweb-mkpkglists = "aurweb.scripts.mkpkglists:main"
aurweb-notify = "aurweb.scripts.notify:main"
aurweb-pkgmaint = "aurweb.scripts.pkgmaint:main"
aurweb-popupdate = "aurweb.scripts.popupdate:main"
aurweb-rendercomment = "aurweb.scripts.rendercomment:main"
aurweb-tuvotereminder = "aurweb.scripts.tuvotereminder:main"
aurweb-usermaint = "aurweb.scripts.usermaint:main"
aurweb-config = "aurweb.scripts.config:main"
aurweb-adduser = "aurweb.scripts.adduser:main"
aurweb-git-archive = "aurweb.scripts.git_archive:main"
